package no.ntnu.kristinejorskogen.gameoflife;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.Random;

/**
 * Class that handles the game logic
 * @author Kristine
 */
public class Game {
    private Context context;
    private int gridWidth = 30;
    private int gridHeight = 30;
    private int[][] currentGrid;
    public TextView[][] grid;
    public TableLayout tableLayout;
    public int liveNeighbours;

    public final Handler handler = new Handler();
    public final Runnable delayRunnable = new Runnable() {
        /**
         * Method that runs the update every second
         */
        @Override
        public void run() {
            updateGrid();
            handler.postDelayed(this, 1000);
        }
    };

    /**
     * Constructor that takes in context and sets parameters for the grid
     * @param appContext Application context
     */
    Game(Context appContext) {
        context = appContext;
        grid = new TextView[gridHeight][gridWidth];
        currentGrid = new int[gridHeight][gridWidth];
        tableLayout = new TableLayout(context);
    }

    /**
     * Method that sets up the grid for the game
     * The grid is table rows with text views
     */
    public void setUpGrid() {

        for (int i = 0; i < gridHeight; i++) {
            TableRow tableRow = new TableRow(context);
            tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
            tableRow.setGravity(Gravity.CENTER);
            tableRow.setBackgroundColor(Color.CYAN);

            for (int j = 0; j < gridWidth; j++) {
                grid[i][j] = new TextView(context);
                grid[i][j].setLayoutParams(new TableRow.LayoutParams(20, 20));
                tableRow.addView(grid[i][j]);
            }

            tableLayout.addView(tableRow);

            //Adds padding to the last row
            if (i == gridHeight - 1) {
                tableRow.setPadding(0, 0, 0, 20);
            }
        }
        newGame();
    }

    /**
     * Method that sets up the board with random number of live cells
     * @param row Row on grid
     * @param col Column on grid
     */
    public void setCells(int row, int col) {
        Random random = new Random();
        int maxRandomCells = 3;
        int randomCellsRow[] = new int[gridHeight];
        int randomCellsCol[] = new int[gridWidth];

        //Sets up the cells as alive (1) or dead (0)
        for (int i = 0; i < gridHeight; i++) {
            randomCellsRow[i] = random.nextInt(maxRandomCells);

            for (int j = 0; j < gridWidth; j++) {
                randomCellsCol[j] = random.nextInt(maxRandomCells);

                if (randomCellsCol[i] == 1 && randomCellsRow[j] == 1) {
                    grid[row][col].setText("*");
                } else {
                    grid[row][col].setText(" ");
                }
            }
        }
    }

    /**
     * Method that updates the game board
     */
    public void updateGrid() {
        boolean alive;

        for (int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {

                //Check amount of live neighbours
                liveNeighbours = getLiveNeighbors(i, j);

                //Is cell alive?
                alive = grid[i][j].getText().equals("*");
                if (alive) {
                    currentGrid[i][j] = 1;
                } else {
                    currentGrid[i][j] = 0;
                }

                // If live neighbours are less than 2 or more than 3, the cell dies
                if (alive && liveNeighbours < 2 || liveNeighbours > 3) {
                    currentGrid[i][j] = 0;
                    grid[i][j].setText(" ");
                }

                //If the cell has exactly 3 live neighbours, it becomes alive
                if (!alive && liveNeighbours == 3) {
                    currentGrid[i][j] = 1;
                    grid[i][j].setText("*");
                }
            }
        }
    }

    /**
     * Checks for live neighbour cells
     * @param row Rows in grid
     * @param col Columns in grid
     * @return Returns amount of live neighbours
     */
    private int getLiveNeighbors(int row, int col) {
        int neighbours = 0;

        //Checks the cells neighbours
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {

                //Making sure we don't get negative numbers
                int updatedColumn = ((j + gridWidth) % gridWidth);
                int updatedRow = ((i + gridHeight) % gridHeight);

                //If cell alive and not the cell we are checking, add to live neighbours
                if (grid[updatedRow][updatedColumn].getText().equals("*")
                        && !(updatedRow == row && updatedColumn == col)) {
                    neighbours++;
                }
            }
        }
        return neighbours;
    }

    /**
     * Sets up cells for a new game
     */
    public void newGame() {
        for (int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {
                setCells(i, j);
            }
        }
    }
}
