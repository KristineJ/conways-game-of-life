package no.ntnu.kristinejorskogen.gameoflife;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

/**
 * Class for the game activity
 * @author Kristine
 */
public class GameActivity extends AppCompatActivity {

    private LinearLayout gameLayout;

    private Context context;
    private Game game;
    private Buttons buttons;

    /**
     * Runs on startup
     * @param savedInstanceState Previous state, if any saved
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set layout
        context = this;
        gameLayout = new LinearLayout(context);
        gameLayout.setOrientation(LinearLayout.VERTICAL);
        gameLayout.setBackgroundColor(Color.DKGRAY);
        setContentView(gameLayout);

        //Create game
        game = new Game(context);
        gameLayout.addView(game.tableLayout);
        game.setUpGrid();

        //Add button layout
        buttons = new Buttons(context);
        gameLayout.addView(buttons.buttonLayout);
        buttons.setupButtons(game);
    }
}
