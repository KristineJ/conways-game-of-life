package no.ntnu.kristinejorskogen.gameoflife;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Class that handles all the buttons for the game
 * @author Kristine
 */
public class Buttons {

    Context context;

    public Button startButton;
    public Button stopButton;
    public Button resetButton;
    public LinearLayout buttonLayout;

    /**
     * Constructor that takes in context and sets parameters for the buttons
     * @param appContext Application context
     */
    Buttons (Context appContext) {
        context = appContext;
        buttonLayout = new LinearLayout(context);
    }

    /**
     * Method that sets up all the buttons
     * @param game The current game object
     */
    public void setupButtons(final Game game) {

        buttonLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        startButton = new Button(context);
        stopButton = new Button(context);
        resetButton = new Button(context);

        buttonLayout.addView(startButton);
        buttonLayout.addView(stopButton);
        buttonLayout.addView(resetButton);

        startButton.setText("Start");
        stopButton.setText("Stop");
        resetButton.setText("Reset game");

        startButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        stopButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        resetButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        startButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Calls the method that runs the update every second
                game.handler.postDelayed(game.delayRunnable, 1000);
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Stops any callbacks to the handler
                game.handler.removeCallbacks(game.delayRunnable);
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Sets up a new game
                game.newGame();
            }
        });
    }
}
